# AleppoJS library
** Automatic Back-End code generation from Schemas , typescript/nodejs **

This library is compatible with: [GraphQL](https://github.com/graphql "GraphQL") [Mongoose](https://github.com/Automattic/mongoose "Mongoose") [Feathersjs](https://github.com/feathersjs "feathersjs") .

### Features:
- Relationship and Rules between Schemas.
- Convert the simple schema before app started to mongo schema object and graph type object.
- Automatically generate basic functions: create, update, get, find, remove, patch, search, onCreate, onDelete, onUpdate.
- Upload files to GridFS storage as Blob and selected as Base64 with reference system.
- Authentication system and access token managed on Redis.










